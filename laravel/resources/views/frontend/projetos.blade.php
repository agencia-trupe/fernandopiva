@extends('frontend.common.template')

@section('content')

<section class="projetos">
    <div class="center">
        @include('frontend.menu-categorias')

        <div class="projetos">
            @foreach($projetos as $projeto)
            <a href="{{ route('projetos.show', $projeto->slug) }}" class="projeto">
                <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" class="img-capa" alt="">
                <div class="overlay">
                    <span class="titulo">{{ $projeto->titulo }}</span>
                    <span class="categoria">{{ $projeto->cat_titulo }}</span>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</section>

@endsection