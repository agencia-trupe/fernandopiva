@extends('frontend.common.template')

@section('content')

<section class="projetos-show">
    <div class="center">
        @include('frontend.menu-categorias-show')

        <div class="dados-projeto">
            <div class="nome-cat">
                <p class="titulo">{{ $projeto->titulo }}</p>
                <p class="categoria">{{ $categoria->titulo }}</p>
            </div>
            <div class="loading">
                <img src="{{ asset('assets/img/layout/load.gif') }}" alt="">
            </div>
            <div class="imagens">
                @foreach($imagens as $imagem)
                <a href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="projeto-imagem" rel="projeto" title="{{ $imagem->descricao }}">
                    <img src="{{ asset('assets/img/projetos/imagens/grid/'.$imagem->imagem) }}" class="img-projeto" alt="{{ $imagem->descricao }}">
                </a>
                @endforeach
            </div>
            @if($projeto->video != null)
            @php
            $linkVideo = "https://www.youtube.com/embed/".$projeto->video;
            $linkYoutube = "https://www.youtube.com/watch?v=".$projeto->video;
            @endphp
            <a href="{{ $linkYoutube }}" target="_blank" class="projeto-video">
                <div class="video">
                    <iframe src="{{ $linkVideo }}"></iframe>
                </div>
            </a>
            @endif
        </div>
        <div class="bolinhas">
            <a href="{{ URL::previous() }}" class="link-voltar" title="Voltar aos Projetos" style="display: none;">
                <img src="{{ asset('assets/img/layout/setinha-topo.svg') }}" alt="" class="img-seta-voltar">
                <p class="texto-voltar">VOLTAR</p>
            </a>
            <a href="" class="link-topo" title="Voltar ao Topo" style="display: none;">
                <img src="{{ asset('assets/img/layout/setinha-topo.svg') }}" alt="" class="img-seta-topo">
                <p class="texto-topo">TOPO</p>
            </a>
        </div>
    </div>
</section>

@endsection