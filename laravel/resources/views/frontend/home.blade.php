@extends('frontend.common.template')

@section('content')

<section class="home">
    <div class="banners">
        @foreach($banners as $banner)
        @if($banner->link)
        <a href="{{ $banner->link }}" class="banner" target="_blank" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})"></a>
        @else
        <div class="banner" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})"></div>
        @endif
        @endforeach
    </div>
</section>

@endsection