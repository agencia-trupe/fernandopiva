@extends('frontend.common.template')

@section('content')

<div class="content politica-de-privacidade">
    <h2 class="titulo">POLÍTICA DE PRIVACIDADE</h2>
    <div class="texto">
        {!! $politica->texto !!}
    </div>
</div>

<div class="direitos">
    <p class="empresa">© {{ date('Y') }} {{ $config->title }} {{ $config->description }} - Todos os direitos reservados | </p>
    <a href="http://www.trupe.net" target="_blank" class="link-trupe">Criação de sites: </a>
    <a href="http://www.trupe.net" target="_blank" class="link-trupe">Trupe Agência Criativa <img src="{{ asset('assets/img/layout/kombi-trupe.svg') }}" alt="" class="img-kombi"></a>
</div>

@endsection