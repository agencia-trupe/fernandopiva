@extends('frontend.common.template')

@section('content')

<section class="projetos-antes-depois-show">
    <div class="center">
        @include('frontend.menu-categorias')

        <div class="dados-projeto">
            <div class="nome-cat">
                <p class="titulo">{{ $projeto->titulo }}</p>
                <p class="categoria">{{ $categoria->titulo }}</p>
            </div>
            <div class="loading">
                <img src="{{ asset('assets/img/layout/load.gif') }}" alt="">
            </div>
            <div class="imagens">
                @foreach($imagens as $imagem)
                <div class="projeto-imagem twentytwenty-container">
                    <img src="{{ asset('assets/img/projetos-antes-depois/imagens/'.$imagem->imagem_antes) }}" class="img-projeto" alt="">
                    <img src="{{ asset('assets/img/projetos-antes-depois/imagens/'.$imagem->imagem_depois) }}" class="img-projeto" alt="">
                </div>
                @endforeach
            </div>
        </div>
        <div class="bolinhas">
            <a href="{{ URL::previous() }}" class="link-voltar" title="Voltar aos Projetos" style="display: none;">
                <img src="{{ asset('assets/img/layout/setinha-topo.svg') }}" alt="" class="img-seta-voltar">
                <p class="texto-voltar">VOLTAR</p>
            </a>
            <a href="" class="link-topo" title="Voltar ao Topo" style="display: none;">
                <img src="{{ asset('assets/img/layout/setinha-topo.svg') }}" alt="" class="img-seta-topo">
                <p class="texto-topo">TOPO</p>
            </a>
        </div>
    </div>
</section>

@endsection