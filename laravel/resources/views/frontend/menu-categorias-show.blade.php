<div class="menu-categorias-show">
    <a href="{{ route('projetos') }}" @if(Tools::routeIs(['projetos', 'projetos.show'])) class="link-categoria active" @endif class="link-categoria">Todos</a>
    @foreach($categorias as $cat)
    <a href="{{ route('projetos.categoria', $cat->id) }}" class="link-categoria" id="categoria{{$cat->id}}">{{ $cat->titulo }}</a>
    @endforeach
    <a href="{{ route('projetos.antes-depois') }}" @if(Tools::routeIs('projetos.antes-depois*')) class="link-categoria active" @endif class="link-categoria">Antes e Depois</a>
</div>