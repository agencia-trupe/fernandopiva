@extends('frontend.common.template')

@section('content')

<section class="contato">
    <div class="center">
        <div class="contato-mapa">
            {!! $contato->google_maps !!}
        </div>

        <div class="contato-dados">
            @php $telefone = str_replace(" ", "", $contato->telefone); @endphp
            <a href="tel:{{ $telefone }}" class="link-telefone"><img src="{{ asset('assets/img/layout/ico-telefone-verde.svg') }}" alt="" class="img-telefone">{{ $contato->telefone }}</a>

            <a href="mailto:{{ $contato->email }}" target="_blank" class="link-email"><img src="{{ asset('assets/img/layout/ico-email-verde.svg') }}" alt="" class="img-email">{{ $contato->email }}</a>

            @php $instagram = "@".str_replace(["https://www.instagram.com/", "/"], "", $contato->instagram); @endphp
            <a href="{{ $contato->instagram }}" class="link-instagram" target="_blank"><img src="{{ asset('assets/img/layout/ico-instagram-verde.svg') }}" alt="" class="img-instagram">{{ $instagram }}</a>

            <a href="https://goo.gl/maps/ZUqUHhBkzEMLhLTv6" target="_blank" class="endereco"><img src="{{ asset('assets/img/layout/ico-endereco-verde.svg') }}" alt="" class="img-endereco">{{ $contato->endereco }}</a>

            <form action="{{ route('contato.post') }}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                {!! Recaptcha::render() !!}
                <button type="submit" class="btn-contato">ENVIAR</button>

                @if($errors->any())
                <div class="flash flash-erro">
                    @foreach($errors->all() as $error)
                    {!! $error !!}<br>
                    @endforeach
                    @if($errors->has('g-recaptcha-response'))
                    <br>Recaptcha inválido.
                    @endif
                </div>
                @endif

                @if(session('enviado'))
                <div class="flash flash-sucesso">
                    <p>Mensagem enviada com sucesso!</p>
                </div>
                @endif
            </form>
        </div>
    </div>

    <div class="direitos">
        <p class="empresa">© {{ date('Y') }} {{ $config->title }} {{ $config->description }} - Todos os direitos reservados | </p>
        <a href="http://www.trupe.net" target="_blank" class="link-trupe">Criação de sites: </a>
        <a href="http://www.trupe.net" target="_blank" class="link-trupe">Trupe Agência Criativa <img src="{{ asset('assets/img/layout/kombi-trupe.svg') }}" alt="" class="img-kombi"></a>
    </div>
</section>

@endsection