<div class="itens-menu">
    <div class="left">
        <a href="{{ route('sobre') }}" @if(Tools::routeIs('sobre*')) class="link-nav active" @endif class="link-nav">SOBRE</a>
        <a href="{{ route('projetos') }}" @if(Tools::routeIs('projetos*')) class="link-nav active" @endif class="link-nav">PROJETOS</a>
    </div>

    <a href="{{ route('home') }}" class="link-logo" title="{{ $config->title }}">
        <img src="{{ asset('assets/img/layout/marca-fernandopiva.svg') }}" alt="" class="img-logo">
    </a>

    <div class="right">
        <a href="{{ route('midia') }}" @if(Tools::routeIs('midia*')) class="link-nav active" @endif class="link-nav">MÍDIA</a>
        <a href="{{ route('contato') }}" @if(Tools::routeIs('contato*')) class="link-nav active" @endif class="link-nav">CONTATO</a>
    </div>
</div>