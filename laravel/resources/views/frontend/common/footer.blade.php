<footer>
    <div class="footer">
        <div class="contatos">
            <a href="https://goo.gl/maps/ZUqUHhBkzEMLhLTv6" target="_blank" class="endereco"><img src="{{ asset('assets/img/layout/ico-endereco.svg') }}" alt="" class="img-endereco">{{ $contato->endereco }}</a>
            
            @php $telefone = str_replace(" ", "", $contato->telefone); @endphp
            <a href="tel:{{ $telefone }}" class="link-telefone"><img src="{{ asset('assets/img/layout/ico-telefone.svg') }}" alt="" class="img-telefone">{{ $contato->telefone }}</a>
            
            <a href="mailto:{{ $contato->email }}" target="_blank" class="email"><img src="{{ asset('assets/img/layout/ico-email.svg') }}" alt="" class="img-email">{{ $contato->email }}</a>
        </div>
        
        @php $instagram = "@".str_replace(["https://www.instagram.com/", "/"], "", $contato->instagram); @endphp
        <a href="{{ $contato->instagram }}" class="link-instagram" target="_blank"><img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="" class="img-instagram">{{ $instagram }}</a>
    </div>
</footer>