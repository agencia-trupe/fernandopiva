@extends('frontend.common.template')

@section('content')

<section class="sobre">
    <div class="dados">
        <div class="img">
            <img src="{{ asset('assets/img/sobre/'.$sobre->imagem) }}" class="img-sobre" alt="">
        </div>
        <div class="texto">
            {!! $sobre->texto !!}
        </div>
    </div>
</section>

@endsection