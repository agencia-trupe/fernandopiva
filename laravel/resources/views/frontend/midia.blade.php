@extends('frontend.common.template')

@section('content')

<section class="midias">
    <div class="center">
        @foreach($midias as $midia)

        @foreach($galerias as $galeria)
        @if($midia->id == $galeria->midia_id)
        <a href="#" class="midia midia-galeria" data-galeria-id="{{ $galeria->id }}">
            <div class="div-img">
                <img src="{{ asset('assets/img/midias/galeria/'.$galeria->capa) }}" class="img-capa" alt="">
            </div>
            <div class="midia-dados">
                <p class="titulo">{{ $midia->titulo }}</p>
                @if($midia->ano)
                <p class="ano">· {{ $midia->ano }}</p>
                @endif
            </div>
        </a>
        @endif
        @endforeach

        @foreach($links as $link)
        @if($midia->id == $link->midia_id)
        <a href="{{ $link->link_ext }}" target="_blank" class="midia">
            <div class="div-img">
                <img src="{{ asset('assets/img/midias/link/'.$link->capa) }}" class="img-capa" alt="">
            </div>
            <div class="midia-dados">
                <p class="titulo">{{ $midia->titulo }}</p>
                @if($midia->ano)
                <p class="ano">· {{ $midia->ano }}</p>
                @endif
            </div>
        </a>
        @endif
        @endforeach

        @foreach($videos as $video)
        @if($midia->id == $video->midia_id)
        @php
        $linkVideo = "https://www.youtube.com/embed/".$video->link_video;
        $linkYoutube = "https://www.youtube.com/watch?v=".$video->link_video;
        @endphp
        <a href="{{ $linkYoutube }}" target="_blank" class="midia">
            <div class="div-img">
                <iframe width="90%" height="60%" src="{{ $linkVideo }}"></iframe>
            </div>
            <div class="midia-dados">
                <p class="titulo">{{ $midia->titulo }}</p>
                @if($midia->ano)
                <p class="ano">· {{ $midia->ano }}</p>
                @endif
            </div>
        </a>
        @endif
        @endforeach

        @endforeach
    </div>
    <button class="btn-mais-midias">VER MAIS +</button>
</section>

<div class="galerias-show" style="display:none;">
    @foreach($galerias as $galeria)
    @foreach($galeria->imagens as $imagem)
    <a href="{{ asset('assets/img/midias/galeria/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria-{{ $galeria->id }}"></a>
    @endforeach
    @endforeach
</div>

@endsection