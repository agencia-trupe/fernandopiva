@extends('frontend.common.template')

@section('content')

<section class="projetos">
    <div class="center">
        @include('frontend.menu-categorias')

        <div class="projetos">
            @foreach($projetos as $projeto)
            <a href="{{ route('projetos.antes-depois.show', $projeto->slug) }}" class="projeto">
                <img src="{{ asset('assets/img/projetos-antes-depois/'.$projeto->capa) }}" class="img-capa" alt="">
                <div class="overlay">
                    <span class="titulo">{{ $projeto->titulo }}</span>
                    <span class="categoria">Antes e Depois</span>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</section>

@endsection