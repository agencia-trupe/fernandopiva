@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('categoria_id', 'Categoria') !!}
    {!! Form::select('categoria_id', $categorias , old('categoria_id'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
    <input type="hidden" name="capaCropped">
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/projetos/'.$projeto->capa) }}" id="imgCapaProjeto" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @else
    <img src="" id="imgCapaProjeto" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    <input type="file" name="capa" class="form-control image" id="inputCapaProjeto">
    <!-- {!! Form::file('capa', ['class' => 'form-control input-capa-projeto']) !!} -->
</div>

<!-- <div class="well form-group">
@if($submitText == 'Alterar')
    <input type="file" name="image" class="image" id="inputCapaProjeto" projeto="capa-{{$projeto->id}}" projetoId="{{$projeto->id}}">
    @else
    <input type="file" name="image" class="image" id="inputCapaProjeto">
    @endif
</div> -->

<div class="form-group">
    {!! Form::label('video', 'Vídeo do YouTube (Opcional)') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
    <p class="observacao" style="color: red; font-style: italic;">Incluir aqui somente a parte da url após o "v=". Exemplo: <span style="text-decoration: underline;">https://www.youtube.com/watch?v=<strong>Y4goaZhNt4k</strong></span></p>
</div>

<div class="well">
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('ativo', 0) !!}
            {!! Form::checkbox('ativo') !!}
            Ativo
        </label>
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.projetos.index') }}" class="btn btn-default btn-voltar">Voltar</a>


<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header" style="display: flex;">
                <h5 class="modal-title" id="modalLabel">CROP - CAPA DO PROJETO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnCloseModal" style="margin: 0 0 0 auto;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-12" style="height: 75vh;">
                            <img id="image" src="#" style="max-height: 100%; max-width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default cke_editable" data-dismiss="modal" id="btnCancelModal">CANCELAR</button>
                <button type="button" class="btn btn-success cke_editable" id="crop">SALVAR</button>
            </div>
        </div>
    </div>
</div>