<div class="imagem col-md-2 col-sm-3 col-xs-4" style="margin:5px 0;position:relative;padding:0 5px;" id="{{ $imagem->id }}" data-ordem="{{ $imagem->ordem or 0 }}" data-imagem="{{ $imagem->imagem }}">
    <input type="checkbox" name="imagens[]" value="{{ $imagem->id }}" style="position:absolute; width:20px; height:20px; margin: 4px 0 0 4px">

    <img src="{{ url('assets/img/projetos/imagens/thumbs/'.$imagem->imagem) }}" alt="" style="display:block;width:100%;height:auto;cursor:move;">
    {!! Form::open([
    'route' => ['painel.projetos.imagens.destroy', $projeto->id, $imagem->id],
    'method' => 'delete'
    ]) !!}

    <div class="btn-group btn-group-sm" style="position:absolute;top:4px;right:10px;">
        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove"></span></button>
    </div>
    {!! Form::close() !!}

    @if($imagem->descricao != null)
    <p class="descricao-img">{{ $imagem->descricao }}</p>
    @endif

    <form action="{{ route('painel.projetos.imagens.descricao', [$projeto->id, $imagem->id]) }}" method="POST" class="form-descricao-img">
        {!! csrf_field() !!}
        <input type="text" class="descricao" name="descricao" value="{{ old('descricao') }}" required>
        <button type="submit" class="btn btn-success btn-sm" id="btnInserirDescricao">inserir/alterar descrição</button>
    </form>
</div>