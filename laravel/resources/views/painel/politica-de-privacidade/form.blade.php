@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto - Política de Privacidade') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBulletLink']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}