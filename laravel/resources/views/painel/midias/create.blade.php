@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Mídias |</small> Adicionar Mídia</h2>
</legend>

{!! Form::open(['route' => 'painel.midias.store', 'files' => true]) !!}

@include('painel.midias.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection