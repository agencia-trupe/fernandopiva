@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Mídias
        <a href="{{ route('painel.midias.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Mídia</a>
    </h2>
</legend>

@if(!count($midias))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="midias">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Capa</th>
            <th>Título</th>
            <th>Ano</th>
            <th>Gerenciar</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($midias as $midia)
        <tr class="tr-row" id="{{ $midia->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>

            @if($midia->tipo_id == 1)
            @foreach($capasGalerias as $capa)
            @if($midia->id == $capa->midia_id)
            <td>
                <img src="{{ asset('assets/img/midias/galeria/'.$capa->capa) }}" style="width: 100%; max-width:60px;" alt="">
            </td>
            @endif
            @endforeach

            @elseif($midia->tipo_id == 2)
            @foreach($capasLinks as $capa)
            @if($midia->id == $capa->midia_id)
            <td>
                <img src="{{ asset('assets/img/midias/link/'.$capa->capa) }}" style="width: 100%; max-width:60px;" alt="">
            </td>
            @endif
            @endforeach

            @elseif($midia->tipo_id == 3)
            @foreach($capasVideos as $capa)
            @php $linkVideo = "https://www.youtube.com/embed/".$capa->link_video; @endphp
            @if($midia->id == $capa->midia_id)
            <td>
                <div class="video"><iframe style="width: 100%; max-width:100px; max-height:60px;" src="{{ $linkVideo }}"></iframe></div>
            </td>
            @endif
            @endforeach

            @else
            <td></td>
            @endif

            <td>{{ $midia->titulo }}</td>
            <td>{{ $midia->ano }}</td>

            <td>
                @foreach($tipos as $tipo)
                @if($tipo->id == $midia->tipo_id && $tipo->id == 1)
                <a href="{{ route('painel.midias.galeria.index', $midia->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>{{ $tipo->titulo }}
                </a>
                @endif
                @if($tipo->id == $midia->tipo_id && $tipo->id == 2)
                <a href="{{ route('painel.midias.link.index', $midia->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>{{ $tipo->titulo }}
                </a>
                @endif
                @if($tipo->id == $midia->tipo_id && $tipo->id == 3)
                <a href="{{ route('painel.midias.video.index', $midia->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>{{ $tipo->titulo }}
                </a>
                @endif
                @endforeach
            </td>

            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.midias.destroy', $midia->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.midias.edit', $midia->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection