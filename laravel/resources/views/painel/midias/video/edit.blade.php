@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Mídia | Vídeo (YouTube) |</small> Editar Vídeo</h2>
</legend>

{!! Form::model($video, [
'route' => ['painel.midias.video.update', $midia->id, $video->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.midias.video.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection