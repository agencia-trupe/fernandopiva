@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Mídia | Vídeo (YouTube) |</small> Adicionar Vídeo</h2>
</legend>

{!! Form::model($midia, [
'route' => ['painel.midias.video.store', $midia->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.midias.video.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection