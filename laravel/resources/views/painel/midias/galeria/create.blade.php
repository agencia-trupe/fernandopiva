@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Mídia | Galeria |</small> Adicionar Galeria</h2>
</legend>

{!! Form::model($midia, [
'route' => ['painel.midias.galeria.store', $midia->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.midias.galeria.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection