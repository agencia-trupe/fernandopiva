@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Mídia | Galeria |</small> Editar Galeria</h2>
</legend>

{!! Form::model($galeria, [
'route' => ['painel.midias.galeria.update', $midia->id, $galeria->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.midias.galeria.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection