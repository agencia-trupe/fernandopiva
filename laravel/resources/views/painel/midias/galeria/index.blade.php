@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Mídia | Galeria
    </h2>
    <a href="{{ route('painel.midias.index') }}" title="Voltar para Mídias" class="btn btn-sm btn-default">
        &larr; Voltar para Mídias</a>
</legend>

@if($galeria == null)
<div class="alert alert-warning" role="alert" style="display: flex; justify-content: space-between; align-items: center;">Nenhum registro encontrado.
    <a href="{{ route('painel.midias.galeria.create', $midia->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Galeria</a>
</div>

@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="midias_galerias">
    <thead>
        <tr>
            <th>Capa</th>
            <th>Título</th>
            <th>Gerenciar</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        <tr class="tr-row" id="{{ $galeria->id }}">
            <td><img src="{{ asset('assets/img/midias/galeria/'.$galeria->capa) }}" style="width: 100%; max-width:80px;" alt=""></td>
            <td>{{ $midia->titulo }}</td>
            <td><a href="{{ route('painel.midias.galeria.imagens.index', [$midia->id, $galeria->id]) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Imagens
                </a></td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.midias.galeria.destroy', $midia->id, $galeria->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.midias.galeria.edit', [$midia->id, $galeria->id] ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
    </tbody>
</table>
@endif

@endsection