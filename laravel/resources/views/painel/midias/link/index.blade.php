@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Mídia | Link Externo
    </h2>
    <a href="{{ route('painel.midias.index') }}" title="Voltar para Mídias" class="btn btn-sm btn-default">
        &larr; Voltar para Mídias</a>
</legend>

@if($link == null)
<div class="alert alert-warning" role="alert" style="display: flex; justify-content: space-between; align-items: center;">Nenhum registro encontrado.
    <a href="{{ route('painel.midias.link.create', $midia->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Link Externo</a>
</div>

@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="midias_links">
    <thead>
        <tr>
            <th>Capa</th>
            <th>Título</th>
            <th>Link</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        <tr class="tr-row" id="{{ $link->id }}">
            <td><img src="{{ asset('assets/img/midias/link/'.$link->capa) }}" style="width: 100%; max-width:80px;" alt=""></td>
            <td>{{ $midia->titulo }}</td>
            <td>{{ $link->link_ext }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.midias.link.destroy', $midia->id, $link->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.midias.link.edit', [$midia->id, $link->id] ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
    </tbody>
</table>
@endif

@endsection