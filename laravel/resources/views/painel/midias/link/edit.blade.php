@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Mídia | Link Externo |</small> Editar Galeria</h2>
</legend>

{!! Form::model($link, [
'route' => ['painel.midias.link.update', $midia->id, $link->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.midias.link.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection