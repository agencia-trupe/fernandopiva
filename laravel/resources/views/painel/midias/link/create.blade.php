@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Mídia | Link Externo |</small> Adicionar Link</h2>
</legend>

{!! Form::model($midia, [
'route' => ['painel.midias.link.store', $midia->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.midias.link.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection