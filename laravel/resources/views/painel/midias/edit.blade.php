@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Mídias |</small> Editar Mídia</h2>
</legend>

{!! Form::model($midia, [
'route' => ['painel.midias.update', $midia->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.midias.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection