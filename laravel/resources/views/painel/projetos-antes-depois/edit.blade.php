@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Projetos | Antes e Depois |</small> Editar Projeto</h2>
    </legend>

    {!! Form::model($projeto, [
        'route'  => ['painel.projetos-antes-depois.update', $projeto->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.projetos-antes-depois.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
