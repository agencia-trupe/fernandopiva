@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Projetos | Antes e Depois |</small> Adicionar Projeto</h2>
    </legend>

    {!! Form::open(['route' => 'painel.projetos-antes-depois.store', 'files' => true]) !!}

        @include('painel.projetos-antes-depois.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
