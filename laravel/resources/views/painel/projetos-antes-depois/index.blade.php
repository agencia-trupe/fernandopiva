@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Projetos | Antes e Depois
        <a href="{{ route('painel.projetos-antes-depois.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Projeto</a>
    </h2>
</legend>

<h4 class="titulo-categorias">Categorias:</h4>
<div class="lista-categorias">
    @foreach($categorias as $categoria)
    <a href="{{ route('painel.projetos-antes-depois.index', ['categoria' => $categoria->id]) }}" class="categoria {{ (isset($_GET['categoria']) && $_GET['categoria'] == $categoria->id) ? 'active' : '' }}">{{ $categoria->titulo }}</a>
    @endforeach
</div>

@if(!count($projetos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="projetos_antes_depois">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Capa</th>
            <th>Título</th>
            <th>Ativo</th>
            <th>Gerenciar</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($projetos as $projeto)
        <tr class="tr-row" id="{{ $projeto->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td><img src="{{ asset('assets/img/projetos-antes-depois/'.$projeto->capa) }}" style="width: 100%; max-width:80px;" alt=""></td>
            <td>{{ $projeto->titulo }}</td>
            <td>
                <span class="glyphicon {{ $projeto->ativo ? 'text-success glyphicon-ok' : 'text-danger glyphicon-remove' }}"></span>
            </td>
            <td><a href="{{ route('painel.projetos-antes-depois.imagens.index', $projeto->id) }}" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Imagens
                </a></td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.projetos-antes-depois.destroy', $projeto->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.projetos-antes-depois.edit', $projeto->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection