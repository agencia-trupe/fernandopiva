@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('categoria_id', 'Categoria') !!}
    {!! Form::select('categoria_id', $categorias , old('categoria_id'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/projetos-antes-depois/'.$projeto->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="well">
    <div class="checkbox" style="margin:0">
        <label style="font-weight:bold">
            {!! Form::hidden('ativo', 0) !!}
            {!! Form::checkbox('ativo') !!}
            Ativo
        </label>
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.projetos-antes-depois.index') }}" class="btn btn-default btn-voltar">Voltar</a>