@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>Projeto: {{ $projeto->titulo }} | Antes e Depois |</small> Adicionar Imagens</h2>
</legend>

{!! Form::model($projeto, [
'route' => ['painel.projetos-antes-depois.imagens.store', $projeto->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.projetos-antes-depois.imagens.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection