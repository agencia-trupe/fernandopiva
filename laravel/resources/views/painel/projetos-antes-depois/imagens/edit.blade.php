@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Projeto: {{ $projeto->titulo }} | Antes e Depois |</small> Editar Imagens</h2>
    </legend>

    {!! Form::model($imagens, [
        'route'  => ['painel.projetos-antes-depois.imagens.update', $projeto->id, $imagens->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.projetos-antes-depois.imagens.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
