@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('tipo', 'Tipo') !!}
    {!! Form::select('tipo', $tipos, old('tipo'), ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_antes', 'Imagem Antes') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/projetos-antes-depois/imagens/'.$imagens->imagem_antes) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_antes', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_depois', 'Imagem Depois') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/projetos-antes-depois/imagens/'.$imagens->imagem_depois) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem_depois', ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.projetos-antes-depois.imagens.index', $projeto->id) }}" class="btn btn-default btn-voltar">Voltar</a>