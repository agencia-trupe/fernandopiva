@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend>
    <h2>
        Projeto: {{ $projeto->titulo }} | Antes e Depois | Imagens
        <a href="{{ route('painel.projetos-antes-depois.imagens.create', $projeto->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Imagens</a>
    </h2>
</legend>

@if(!count($imagens))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="projetos_antes_depois_imagens">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Imagem Antes</th>
            <th>Imagem Depois</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($imagens as $imagem)
        <tr class="tr-row" id="{{ $imagem->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td><img src="{{ asset('assets/img/projetos-antes-depois/imagens/'.$imagem->imagem_antes) }}" style="width: auto; max-height:80px;" alt=""></td>
            <td><img src="{{ asset('assets/img/projetos-antes-depois/imagens/'.$imagem->imagem_depois) }}" style="width: auto; max-height:80px;" alt=""></td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.projetos-antes-depois.imagens.destroy', $projeto->id, $imagem->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('painel.projetos-antes-depois.imagens.edit', [$projeto->id, $imagem->id] ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection