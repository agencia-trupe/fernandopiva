// var toolbarGroups = [
// 		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
// 		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
// 		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
// 		{ name: 'forms', groups: [ 'forms' ] },
// 		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
// 		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
// 		{ name: 'links', groups: [ 'links' ] },
// 		{ name: 'insert', groups: [ 'insert' ] },
// 		'/',
// 		{ name: 'styles', groups: [ 'styles' ] },
// 		{ name: 'colors', groups: [ 'colors' ] },
// 		{ name: 'tools', groups: [ 'tools' ] },
// 		{ name: 'others', groups: [ 'others' ] },
// 		{ name: 'about', groups: [ 'about' ] }
// 	];

// var removeButtons = 'Source,Save,NewPage,ExportPdf,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Undo,Redo,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Strike,Subscript,Superscript,CopyFormatting,RemoveFormat,Blockquote,CreateDiv,BidiLtr,BidiRtl,Language,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Image,Styles,Font,Maximize,ShowBlocks,About';

const config = {
  padrao: {
    toolbar: [["Bold", "Italic"]],
  },

  clean: {
    toolbar: [],
    removePlugins: "toolbar,elementspath",
  },

  cleanBr: {
    toolbar: [],
    removePlugins: "toolbar,elementspath",
    enterMode: CKEDITOR.ENTER_BR,
  },

  textEdit: {
    toolbar: [
      ["Bold", "Italic"],
      ["Format"],
      ["TextColor", "BGColor"],
      ["BulletedList", "NumberedList"],
      ["Link", "Unlink"],
      ["InjectImage"],
    ],
  },

  textBulletLink: {
    toolbar: [["Bold", "Italic"], ["BulletedList"], ["Link", "Unlink"]],
  },
};

export default function TextEditor() {
  CKEDITOR.config.language = "pt-br";
  CKEDITOR.config.uiColor = "#dce4ec";
  CKEDITOR.config.contentsCss = [
    $("base").attr("href") + "/assets/ckeditor.css",
    CKEDITOR.config.contentsCss,
  ];
  CKEDITOR.config.removePlugins = "elementspath";
  CKEDITOR.config.resize_enabled = false;
  CKEDITOR.plugins.addExternal(
    "injectimage",
    $("base").attr("href") + "/assets/injectimage/plugin.js"
  );
  CKEDITOR.config.allowedContent = true;
  CKEDITOR.config.extraPlugins = "injectimage,colorbutton";

  $(".ckeditor").each(function (i, obj) {
    CKEDITOR.replace(obj.id, config[obj.dataset.editor]);
  });
}
