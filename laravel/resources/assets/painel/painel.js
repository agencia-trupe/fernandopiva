import Clipboard from "./modules/Clipboard.js";
import DataTables from "./modules/DataTables.js";
import DatePicker from "./modules/DatePicker.js";
import DeleteButton from "./modules/DeleteButton.js";
import Filtro from "./modules/Filtro.js";
import GeneratorFields from "./modules/GeneratorFields.js";
import ImagesUpload from "./modules/ImagesUpload.js";
import MonthPicker from "./modules/MonthPicker.js";
import MultiSelect from "./modules/MultiSelect.js";
import OrderImages from "./modules/OrderImages.js";
import OrderTable from "./modules/OrderTable.js";
import TextEditor from "./modules/TextEditor.js";

$.ajaxSetup({
  headers: {
    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
  },
});

Clipboard();
DataTables();
DatePicker();
DeleteButton();
Filtro();
GeneratorFields();
ImagesUpload();
MonthPicker();
MultiSelect();
OrderImages();
OrderTable();
TextEditor();

var urlPrevia = "";
// var urlPrevia = "/previa-fernandopiva";

$(".btn-excluir-imgs").click(function (e) {
  e.preventDefault();

  var projetoId = $(this).attr("projeto");

  var imagemsSelecionadas = $("input:checkbox:checked")
    .map(function () {
      return $(this).val();
    })
    .get();
  console.log(imagemsSelecionadas);

  $.ajax({
    type: "DELETE",
    url:
      window.location.origin +
      urlPrevia +
      "/painel/projetos/" +
      projetoId +
      "/excluir-imagens/" +
      imagemsSelecionadas,
    beforeSend: function () {},
    success: function (data, textStatus, jqXHR) {
      $("input:checkbox:checked").parent().remove();
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR, textStatus, errorThrown);
    },
  });
});

var $modal = $("#modal");
var image = $("#image")[0];
var cropper;

$("body").on("change", ".image", function (e) {
  var files = e.target.files;
  var done = function (url) {
    image.src = url;
    $modal.modal("show");
  };
  var reader;
  var file;

  if (files && files.length > 0) {
    file = files[0];

    if (URL) {
      done(URL.createObjectURL(file));
    } else if (FileReader) {
      reader = new FileReader();
      reader.onload = function (e) {
        done(reader.result);
      };
      reader.readAsDataURL(file);
    }
  }
});

$modal
  .on("shown.bs.modal", function () {
    //console.log('modal show 1', cropper);
    cropper = new Cropper(image, {
      // aspectRatio: 2,
      viewMode: 0,
      center: true,
      autoCropArea: 1,
      minCropBoxWidth: 400,
      minCropBoxHeight: 400,
      minContainerWidth: 400,
      minContainerHeight: 400,
      minCanvasWidth: 400,
      minCanvasHeight: 400,
    });
    //console.log('modal show 2', cropper);
  })
  .on("hidden.bs.modal", function () {
    //console.log('modal hide 1', cropper);
    cropper.destroy();
    cropper = null;
    $(".image").val("");
    //console.log('modal hide 2', cropper);
  });

$("#crop").click(function () {
  // console.log($(this));
  var canvas = cropper.getCroppedCanvas({
    maxWidth: 800,
    maxHeight: 800
  });

  canvas.toBlob(function (blob) {
    var url = URL.createObjectURL(blob);
    var reader = new FileReader();
    reader.readAsDataURL(blob);
    reader.onloadend = function () {
      var base64data = reader.result;

      $("#imgCapaProjeto").attr("src", base64data);

      $($("input[name='capaCropped']")[0]).val(base64data);

      $modal.modal("hide");
    };
  });
});
