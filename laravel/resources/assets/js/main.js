import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

AjaxSetup();
MobileToggle();

$(window).on("load", function () {
  // GRID IMAGENS PROJETOS
  $(".projetos-show .projeto-imagem").each(function () {
    if ($(this).find("img").width() / $(this).find("img").height() > 1) {
      $(this).addClass("horizontal");
    } else {
      $(this).addClass("vertical");
    }
  });

  $(".loading").hide();
  $(".projetos-show .imagens, .projetos-antes-depois-show .imagens").css({
    visibility: "visible",
    "max-height": "100%",
  });

  // projetos - antes e depois
  var itens = $(".projetos-antes-depois-show .projeto-imagem");

  itens.each(function (i) {
    var width = $(itens[i]).children(".img-projeto").eq(0).width();
    $(itens[i]).eq(0).width(width);
  });

  $(".projetos-antes-depois-show .projeto-imagem").twentytwenty({
    default_offset_pct: 0.5,
    before_label: "antes",
    after_label: "depois",
    orientation: "horizontal",
    no_overlay: false,
  });
});

$(document).ready(function () {
  // HOME - BANNERS
  $(".banners").cycle({
    slides: ".banner",
    fx: "fade",
    speed: 400,
  });

  // FOOTER E NAV NA HOME
  if (window.location.href == routeHome) {
    $("footer").css("position", "absolute");
    $("header .itens-menu").css("align-items", "flex-start");
    $("header .itens-menu .link-nav").css("margin-top", "20px");
  }

  // TROCA ACTIVE MENU CATEGORIAS (PROJETOS)
  var categoriaId = localStorage.getItem("categoriaId");

  if (categoriaId) {
    $(".menu-categorias .link-categoria#" + categoriaId).addClass("active");
  }

  $(".menu-categorias .link-categoria").click(function (e) {
    e.preventDefault();
    localStorage.setItem("categoriaId", $(this).attr("id"));
    window.location.href = $(this).attr("href");
  });
  $("header .itens-menu .link-nav").click(function () {
    localStorage.removeItem("categoriaId");
  });

  // FANCYBOX MIDIAS GALERIAS
  $(".fancybox, .projetos-show .imagens a").fancybox({
    padding: 0,
    prevEffect: "fade",
    nextEffect: "fade",
    closeBtn: false,
    openEffect: "elastic",
    openSpeed: "150",
    closeEffect: "elastic",
    closeSpeed: "150",
    keyboard: true,
    helpers: {
      title: {
        type: "outside",
        position: "top",
      },
      overlay: {
        css: {
          background: "rgba(132, 134, 136, .88)",
        },
      },
    },
    mobile: {
      clickOutside: "close",
    },
    fitToView: false,
    autoSize: false,
    beforeShow: function () {
      this.maxWidth = "100%";
      this.maxHeight = "100%";
    },
  });

  $(".midia-galeria").click(function handler(e) {
    e.preventDefault();

    const id = $(this).data("galeria-id");

    if (id) {
      $(`a[rel=galeria-${id}]`)[0].click();
    }
  });

  // BTN VER MAIS - MIDIAS
  var itensMidias = $("section.midias .midia");
  var spliceItensQuantidade = 16;

  if (itensMidias.length <= spliceItensQuantidade) {
    $(".btn-mais-midias").hide();
  }

  var setDivMidias = function () {
    var spliceItens = itensMidias.splice(0, spliceItensQuantidade);
    $("section.midias .center").append(spliceItens);
    $(spliceItens).show();
    if (itensMidias.length <= 0) {
      $(".btn-mais-midias").hide();
    }
  };

  $(".btn-mais-midias").click(function () {
    setDivMidias();
  });

  $("section.midias .midia").hide();
  setDivMidias();

  // PROJETOS/IMAGENS - BTN SCROLL TOP
  $(document).scroll(function (e) {
    e.preventDefault();
    $(".link-topo").toggle($(document).scrollTop() !== 0);
    $(".link-voltar").toggle($(document).scrollTop() !== 0);
  });
  $(".link-topo").click(function (e) {
    e.preventDefault();
    $("html, body").animate({ scrollTop: $("body").offset().top }, 500);
  });

  // AVISO DE COOKIES
  $(".aviso-cookies").hide();

  if (window.location.href == routeHome) {
    $(".aviso-cookies").show();

    $(".aceitar-cookies").click(function () {
      var url = window.location.origin + "/aceite-de-cookies";

      $.ajax({
        type: "POST",
        url: url,
        success: function (data, textStatus, jqXHR) {
          $(".aviso-cookies").hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    });
  }
});
