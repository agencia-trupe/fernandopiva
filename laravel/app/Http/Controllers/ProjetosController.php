<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Models\Projeto;
use App\Models\ProjetoAntesDepois;
use App\Models\ProjetoAntesDepoisImagem;
use App\Models\ProjetoImagem;

class ProjetosController extends Controller
{
    public function index()
    {
        $categorias = Categoria::ordenados()->get();

        $projetos = Projeto::ordenados()
            ->join('categorias', 'categorias.id', '=', 'projetos.categoria_id')
            ->select('projetos.id as id', 'projetos.titulo as titulo', 'projetos.capa as capa', 'projetos.ordem as ordem', 'projetos.slug as slug', 'categorias.titulo as cat_titulo')
            ->where('ativo', 1)->get();

        return view('frontend.projetos', compact('categorias', 'projetos'));
    }

    public function showImagens($projeto_slug)
    {
        $categorias = Categoria::ordenados()->get();

        $projeto = Projeto::where('slug', $projeto_slug)->first();
        $categoria = Categoria::where('id', $projeto->categoria_id)->first();
        $imagens = ProjetoImagem::where('projeto_id', $projeto->id)->ordenados()->get();

        return view('frontend.projetos-show', compact('categorias', 'projeto', 'categoria', 'imagens'));
    }

    public function showCategoria(Categoria $categoria)
    {
        $categorias = Categoria::ordenados()->get();

        $projetos = Projeto::where('categoria_id', $categoria->id)->where('ativo', 1)->ordenados()->get();

        return view('frontend.projetos-categoria', compact('categorias', 'projetos', 'categoria'));
    }

    public function showCategoriaImagens(Categoria $categoria, $projeto_slug)
    {
        $categorias = Categoria::ordenados()->get();

        $projeto = Projeto::where('slug', $projeto_slug)->first();
        $imagens = ProjetoImagem::where('projeto_id', $projeto->id)->ordenados()->get();

        return view('frontend.projetos-categoria-show', compact('categorias', 'projeto', 'categoria', 'imagens'));
    }

    public function indexAntesDepois()
    {
        $categorias = Categoria::ordenados()->get();

        $projetos = ProjetoAntesDepois::where('ativo', 1)->ordenados()->get();

        return view('frontend.projetos-antes-depois', compact('categorias', 'projetos'));
    }

    public function showAntesDepois($projeto_slug)
    {
        $categorias = Categoria::ordenados()->get();

        $projeto = ProjetoAntesDepois::where('slug', $projeto_slug)->first();
        $categoria = Categoria::where('id', $projeto->categoria_id)->first();
        $imagens = ProjetoAntesDepoisImagem::where('projeto_id', $projeto->id)->ordenados()->get();

        return view('frontend.projetos-antes-depois-show', compact('categorias', 'projeto', 'categoria', 'imagens'));
    }
}
