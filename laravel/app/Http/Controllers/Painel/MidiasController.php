<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\MidiasRequest;
use App\Models\Midia;
use App\Models\MidiaGaleria;
use App\Models\MidiaTipo;

class MidiasController extends Controller
{
    public function index()
    {
        $tipos = MidiaTipo::orderBy('id', 'ASC')->get();

        $midias = Midia::ordenados()->get();

        $capasLinks = Midia::join('midias_links', 'midias_links.midia_id', '=', 'midias.id')
            ->select('midias_links.midia_id as midia_id', 'midias.tipo_id as tipo_id', 'midias_links.capa as capa')
            ->get();
        $capasGalerias = Midia::join('midias_galerias', 'midias_galerias.midia_id', '=', 'midias.id')
            ->select('midias_galerias.midia_id as midia_id', 'midias.tipo_id as tipo_id', 'midias_galerias.capa as capa')
            ->get();
        $capasVideos = Midia::join('midias_videos', 'midias_videos.midia_id', '=', 'midias.id')
            ->select('midias_videos.midia_id as midia_id', 'midias.tipo_id as tipo_id', 'midias_videos.link_video as link_video')
            ->get();

        return view('painel.midias.index', compact('midias', 'tipos', 'capasGalerias', 'capasLinks', 'capasVideos'));
    }

    public function create()
    {
        $tipos = MidiaTipo::orderBy('id', 'ASC')->pluck('titulo', 'id');

        return view('painel.midias.create', compact('tipos'));
    }

    public function store(MidiasRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            Midia::create($input);

            return redirect()->route('painel.midias.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Midia $midia)
    {
        $tipos = MidiaTipo::orderBy('id', 'ASC')->pluck('titulo', 'id');

        return view('painel.midias.edit', compact('midia', 'tipos'));
    }

    public function update(MidiasRequest $request, Midia $midia)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            $midia->update($input);

            return redirect()->route('painel.midias.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Midia $midia)
    {
        try {
            $midia->delete();

            return redirect()->route('painel.midias.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
