<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\MidiasVideosRequest;
use App\Models\Midia;
use App\Models\MidiaVideo;

class MidiasVideosController extends Controller
{
    public function index(Midia $midia)
    {
        $video = MidiaVideo::where('midia_id', $midia->id)->first();

        return view('painel.midias.video.index', compact('midia', 'video'));
    }

    public function create(Midia $midia)
    {
        return view('painel.midias.video.create', compact('midia'));
    }

    public function store(MidiasVideosRequest $request, Midia $midia)
    {
        try {
            $input = $request->all();
            $input['midia_id'] = $midia->id;

            MidiaVideo::create($input);

            return redirect()->route('painel.midias.video.index', $midia->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Midia $midia, MidiaVideo $video)
    {
        return view('painel.midias.video.edit', compact('midia', 'video'));
    }

    public function update(MidiasVideosRequest $request, Midia $midia, MidiaVideo $video)
    {
        try {
            $input = $request->all();
            $input['midia_id'] = $midia->id;

            $video->update($input);

            return redirect()->route('painel.midias.video.index', $midia->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Midia $midia, MidiaVideo $video)
    {
        try {
            $video->delete();

            return redirect()->route('painel.midias.video.index', $midia->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
