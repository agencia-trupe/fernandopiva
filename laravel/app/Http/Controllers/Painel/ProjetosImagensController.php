<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjetosImagensRequest;
use App\Models\Projeto;
use App\Models\ProjetoImagem;
use Illuminate\Http\Request;

class ProjetosImagensController extends Controller
{
    public function index(Projeto $projeto)
    {
        $imagens = ProjetoImagem::projeto($projeto->id)->ordenados()->get();

        return view('painel.projetos.imagens.index', compact('imagens', 'projeto'));
    }

    public function show(Projeto $projeto, ProjetoImagem $imagem)
    {
        return $imagem;
    }

    public function store(Projeto $projeto, ProjetosImagensRequest $request)
    {
        try {
            $input = $request->all();
            $input['imagem'] = ProjetoImagem::uploadImagem();
            $input['projeto_id'] = $projeto->id;

            $imagem = ProjetoImagem::create($input);

            $view = view('painel.projetos.imagens.imagem', compact('projeto', 'imagem'))->render();

            return response()->json(['body' => $view]);
        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function destroy(Projeto $projeto, ProjetoImagem $imagem)
    {
        try {
            $imagem->delete();

            return redirect()->route('painel.projetos.imagens.index', $projeto)
                ->with('success', 'Imagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }

    public function clear(Projeto $projeto)
    {
        try {
            $projeto->imagens()->delete();

            return redirect()->route('painel.projetos.imagens.index', $projeto)
                ->with('success', 'Imagens excluídas com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }

    public function destroyImgs(Projeto $projeto, $imagens)
    {
        try {
            ProjetoImagem::projeto($projeto->id)->whereIn('id', explode(",",$imagens))->delete();

            return response()->json(['success' => 'Imagens excluídas com sucesso.']);
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: ' . $e->getMessage()]);
        }
    }

    public function updateDescricao(Request $request, Projeto $projeto, ProjetoImagem $imagem)
    {
        try {
            $input['descricao'] = $request->descricao;

            $imagem->update($input);

            return redirect()->route('painel.projetos.imagens.index', $projeto->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
