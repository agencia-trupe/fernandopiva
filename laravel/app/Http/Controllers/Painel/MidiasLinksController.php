<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\MidiasLinksRequest;
use App\Models\Midia;
use App\Models\MidiaLink;

class MidiasLinksController extends Controller
{
    public function index(Midia $midia)
    {
        $link = MidiaLink::where('midia_id', $midia->id)->first();

        return view('painel.midias.link.index', compact('midia', 'link'));
    }

    public function create(Midia $midia)
    {
        return view('painel.midias.link.create', compact('midia'));
    }

    public function store(MidiasLinksRequest $request, Midia $midia)
    {
        try {
            $input = $request->all();
            $input['midia_id'] = $midia->id;

            if (isset($input['capa'])) $input['capa'] = MidiaLink::upload_capa();

            MidiaLink::create($input);

            return redirect()->route('painel.midias.link.index', $midia->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Midia $midia, MidiaLink $link)
    {
        return view('painel.midias.link.edit', compact('midia', 'link'));
    }

    public function update(MidiasLinksRequest $request, Midia $midia, MidiaLink $link)
    {
        try {
            $input = $request->all();
            $input['midia_id'] = $midia->id;

            if (isset($input['capa'])) $input['capa'] = MidiaLink::upload_capa();

            $link->update($input);

            return redirect()->route('painel.midias.link.index', $midia->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Midia $midia, MidiaLink $link)
    {
        try {
            $link->delete();

            return redirect()->route('painel.midias.link.index', $midia->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
