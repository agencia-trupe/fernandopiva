<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjetosAntesDepoisImagensRequest;
use App\Models\ProjetoAntesDepois;
use App\Models\ProjetoAntesDepoisImagem;

class ProjetosAntesDepoisImagensController extends Controller
{
    public function index(ProjetoAntesDepois $projeto)
    {
        $imagens = ProjetoAntesDepoisImagem::projeto($projeto->id)->ordenados()->get();

        return view('painel.projetos-antes-depois.imagens.index', compact('imagens', 'projeto'));
    }

    public function create(ProjetoAntesDepois $projeto)
    {
        $tipos = [
            'vertical' => 'Vertical',
            'horizontal' => 'Horizontal'
        ];

        return view('painel.projetos-antes-depois.imagens.create', compact('projeto', 'tipos'));
    }

    public function store(ProjetosAntesDepoisImagensRequest $request, ProjetoAntesDepois $projeto)
    {
        try {
            // dd($request);
            $input = $request->all();
            $input['projeto_id'] = $projeto->id;
            $input['tipo'] = $request->tipo;

            if ($request->tipo == 'vertical') {
                if (isset($input['imagem_antes'])) $input['imagem_antes'] = ProjetoAntesDepoisImagem::uploadImagemVerticalAntes();
                if (isset($input['imagem_depois'])) $input['imagem_depois'] = ProjetoAntesDepoisImagem::uploadImagemVerticalDepois();

                ProjetoAntesDepoisImagem::create($input);
            }

            if ($request->tipo == 'horizontal') {
                if (isset($input['imagem_antes'])) $input['imagem_antes'] = ProjetoAntesDepoisImagem::uploadImagemHorizontalAntes();
                if (isset($input['imagem_depois'])) $input['imagem_depois'] = ProjetoAntesDepoisImagem::uploadImagemHorizontalDepois();

                ProjetoAntesDepoisImagem::create($input);
            }

            return redirect()->route('painel.projetos-antes-depois.imagens.index', $projeto->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(ProjetoAntesDepois $projeto, ProjetoAntesDepoisImagem $imagens)
    {
        $tipos = [
            'vertical' => 'Vertical',
            'horizontal' => 'Horizontal'
        ];

        return view('painel.projetos-antes-depois.imagens.edit', compact('projeto', 'imagens', 'tipos'));
    }

    public function update(ProjetosAntesDepoisImagensRequest $request, ProjetoAntesDepois $projeto, ProjetoAntesDepoisImagem $imagens)
    {
        try {
            $input = $request->except('tipo');

            if ($request->tipo == 'vertical') {
                if (isset($input['imagem_antes'])) $input['imagem_antes'] = ProjetoAntesDepoisImagem::uploadImagemVerticalAntes();
                if (isset($input['imagem_depois'])) $input['imagem_depois'] = ProjetoAntesDepoisImagem::uploadImagemVerticalDepois();

                $imagens->update($input);
            }

            if ($request->tipo == 'horizontal') {
                if (isset($input['imagem_antes'])) $input['imagem_antes'] = ProjetoAntesDepoisImagem::uploadImagemHorizontalAntes();
                if (isset($input['imagem_depois'])) $input['imagem_depois'] = ProjetoAntesDepoisImagem::uploadImagemHorizontalDepois();

                $imagens->update($input);
            }

            return redirect()->route('painel.projetos-antes-depois.imagens.index', $projeto->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ProjetoAntesDepois $projeto, ProjetoAntesDepoisImagem $imagens)
    {
        try {
            $imagens->delete();

            return redirect()->route('painel.projetos-antes-depois.imagens.index', $projeto->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
