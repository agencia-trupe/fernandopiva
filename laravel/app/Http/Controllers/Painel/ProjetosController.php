<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjetosRequest;
use App\Models\Categoria;
use App\Models\Projeto;

class ProjetosController extends Controller
{
    public function index()
    {
        $categorias = Categoria::ordenados()->get();

        if (isset($_GET['categoria'])) {
            $categoria = $_GET['categoria'];
            $projetos = Projeto::where('categoria_id', $categoria)->ordenados()->get();
        } else {
            $projetos = Projeto::ordenados()->get();
        }

        return view('painel.projetos.index', compact('projetos', 'categorias'));
    }

    public function create()
    {
        $categorias = Categoria::ordenados()->pluck('titulo', 'id');

        return view('painel.projetos.create', compact('categorias'));
    }

    public function store(ProjetosRequest $request)
    {
        try {
            $input['titulo'] = $request->titulo;
            $input['categoria_id'] = $request->categoria_id;
            $input['video'] = $request->video;
            $input['ativo'] = $request->ativo;
            $input['slug'] = str_slug($request->titulo, '-');

            if (!empty($request->capaCropped)) {
                $folderPath = public_path("assets/img/projetos/");
                $image_parts = explode(";base64,", $request->capaCropped);
                $image_base64 = base64_decode($image_parts[1]);
                $filename = uniqid() . '.jpg';
                $file = $folderPath . $filename;
                file_put_contents($file, $image_base64);

                $input['capa'] = $filename;
            }

            Projeto::create($input);

            return redirect()->route('painel.projetos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Projeto $projeto)
    {
        $categorias = Categoria::ordenados()->pluck('titulo', 'id');

        return view('painel.projetos.edit', compact('projeto', 'categorias'));
    }

    public function update(ProjetosRequest $request, Projeto $projeto)
    {
        try {
            $projeto->slug = str_slug($request->titulo, '-');
            $projeto->titulo = $request->titulo;
            $projeto->categoria_id = $request->categoria_id;
            $projeto->video = $request->video;
            $projeto->ativo = $request->ativo;

            if (!empty($request->capaCropped)) {
                $folderPath = public_path("assets/img/projetos/");
                $image_parts = explode(";base64,", $request->capaCropped);
                $image_base64 = base64_decode($image_parts[1]);
                $filename = uniqid() . '.jpg';
                $file = $folderPath . $filename;
                file_put_contents($file, $image_base64);

                $projeto->capa = $filename;
            }

            $projeto->save();

            return redirect()->route('painel.projetos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Projeto $projeto)
    {
        try {
            $projeto->delete();

            return redirect()->route('painel.projetos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
