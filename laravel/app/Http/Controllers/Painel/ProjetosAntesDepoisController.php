<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjetosAntesDepoisRequest;
use App\Models\Categoria;
use App\Models\ProjetoAntesDepois;

class ProjetosAntesDepoisController extends Controller
{
    public function index()
    {
        $categorias = Categoria::ordenados()->get();

        if (isset($_GET['categoria'])) {
            $categoria = $_GET['categoria'];
            $projetos = ProjetoAntesDepois::where('categoria_id', $categoria)->ordenados()->get();
        } else {
            $projetos = ProjetoAntesDepois::ordenados()->get();
        }

        return view('painel.projetos-antes-depois.index', compact('projetos', 'categorias'));
    }

    public function create()
    {
        $categorias = Categoria::orderBy('id', 'ASC')->pluck('titulo', 'id');

        return view('painel.projetos-antes-depois.create', compact('categorias'));
    }

    public function store(ProjetosAntesDepoisRequest $request)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            if (isset($input['capa'])) $input['capa'] = ProjetoAntesDepois::upload_capa();

            ProjetoAntesDepois::create($input);

            return redirect()->route('painel.projetos-antes-depois.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(ProjetoAntesDepois $projeto)
    {
        $categorias = Categoria::orderBy('id', 'ASC')->pluck('titulo', 'id');

        return view('painel.projetos-antes-depois.edit', compact('projeto', 'categorias'));
    }

    public function update(ProjetosAntesDepoisRequest $request, ProjetoAntesDepois $projeto)
    {
        try {
            $input = $request->all();
            $input['slug'] = str_slug($request->titulo, '-');

            if (isset($input['capa'])) $input['capa'] = ProjetoAntesDepois::upload_capa();

            $projeto->update($input);

            return redirect()->route('painel.projetos-antes-depois.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(ProjetoAntesDepois $projeto)
    {
        try {
            $projeto->delete();

            return redirect()->route('painel.projetos-antes-depois.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
