<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\MidiasGaleriasRequest;
use App\Models\Midia;
use App\Models\MidiaGaleria;

class MidiasGaleriasController extends Controller
{
    public function index(Midia $midia)
    {
        $galeria = MidiaGaleria::where('midia_id', $midia->id)->first();

        return view('painel.midias.galeria.index', compact('midia', 'galeria'));
    }

    public function create(Midia $midia)
    {
        return view('painel.midias.galeria.create', compact('midia'));
    }

    public function store(MidiasGaleriasRequest $request, Midia $midia)
    {
        try {
            $input = $request->all();
            $input['midia_id'] = $midia->id;

            if (isset($input['capa'])) $input['capa'] = MidiaGaleria::upload_capa();

            MidiaGaleria::create($input);

            return redirect()->route('painel.midias.galeria.index', $midia->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Midia $midia, MidiaGaleria $galeria)
    {
        return view('painel.midias.galeria.edit', compact('midia', 'galeria'));
    }

    public function update(MidiasGaleriasRequest $request, Midia $midia, MidiaGaleria $galeria)
    {
        try {
            $input = $request->all();
            $input['midia_id'] = $midia->id;

            if (isset($input['capa'])) $input['capa'] = MidiaGaleria::upload_capa();

            $galeria->update($input);

            return redirect()->route('painel.midias.galeria.index', $midia->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Midia $midia, MidiaGaleria $galeria)
    {
        try {
            $galeria->delete();

            return redirect()->route('painel.midias.galeria.index', $midia->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
