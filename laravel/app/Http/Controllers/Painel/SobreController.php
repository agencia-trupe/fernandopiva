<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\SobreRequest;
use App\Models\Sobre;

class SobreController extends Controller
{
    public function index()
    {
        $registro = Sobre::first();

        return view('painel.sobre.edit', compact('registro'));
    }

    public function update(SobreRequest $request, Sobre $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Sobre::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.sobre.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
