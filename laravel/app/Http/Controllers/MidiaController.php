<?php

namespace App\Http\Controllers;

use App\Models\Midia;
use App\Models\MidiaGaleria;
use App\Models\MidiaLink;
use App\Models\MidiaVideo;

class MidiaController extends Controller
{
    public function index()
    {
        $midias = Midia::ordenados()
            ->join('midias_tipos', 'midias_tipos.id', '=', 'midias.tipo_id')
            ->select('midias_tipos.id as tipo_id', 'midias_tipos.titulo as tipo_titulo', 'midias.id as id', 'midias.ordem as ordem', 'midias.slug as slug', 'midias.titulo as titulo', 'midias.ano as ano')
            ->get();

        $galerias = MidiaGaleria::get();
        $links = MidiaLink::get();
        $videos = MidiaVideo::get();

        return view('frontend.midia', compact('midias', 'galerias', 'links', 'videos'));
    }
}
