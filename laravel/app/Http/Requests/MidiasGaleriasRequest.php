<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MidiasGaleriasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'capa'   => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
