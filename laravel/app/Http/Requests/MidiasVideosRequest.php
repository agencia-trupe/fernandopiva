<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MidiasVideosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'link_video' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
        ];
    }
}
