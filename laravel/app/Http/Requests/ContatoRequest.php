<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'        => 'required',
            'email'       => 'required|email',
            'telefone'    => 'required',
            'endereco'    => 'required',
            'instagram'   => 'required',
            'google_maps' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => "Preencha todos os campos corretamente.",
            'email'    => "Insira um endereço de e-mail válido.",
        ];
    }
}
