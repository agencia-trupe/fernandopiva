<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatosRecebidosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'     => 'required',
            'email'    => 'required|email',
            'mensagem' => 'required',
            'g-recaptcha-response' => 'required|recaptcha'
        ];
    }

    public function messages()
    {
        return [
            'required'  => "Preencha todos os campos corretamente.",
            'email'     => "Insira um endereço de e-mail válido.",
            'recaptcha' => "Por favor, confirme que você é um humano!"
        ];
    }
}
