<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MidiasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required',
            'ano'    => ''
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
        ];
    }
}
