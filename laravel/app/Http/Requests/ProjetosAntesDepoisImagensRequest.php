<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjetosAntesDepoisImagensRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem_antes' => 'required|image',
            'imagem_depois' => 'required|image'
        ];

        if ($this->method() != 'POST') {
            $rules['imagem_antes'] = 'image';
            $rules['imagem_depois'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
        ];
    }
}
