<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('sobre', 'SobreController@index')->name('sobre');
    Route::get('projetos/antes-e-depois', 'ProjetosController@indexAntesDepois')->name('projetos.antes-depois');
    Route::get('projetos/antes-e-depois/{projeto}', 'ProjetosController@showAntesDepois')->name('projetos.antes-depois.show');
    Route::get('projetos', 'ProjetosController@index')->name('projetos');
    Route::get('projetos/{projeto}', 'ProjetosController@showImagens')->name('projetos.show');
    Route::get('projetos/categoria/{categoria}', 'ProjetosController@showCategoria')->name('projetos.categoria');
    Route::get('projetos/{categoria}/{projeto}', 'ProjetosController@showCategoriaImagens')->name('projetos.categoria.show');
    Route::get('midia', 'MidiaController@index')->name('midia');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        Route::resource('banners', 'BannersController');
        Route::resource('sobre', 'SobreController', ['only' => ['index', 'update']]);
        Route::resource('projetos', 'ProjetosController', ['except' => 'show']);
        Route::get('projetos/{projeto}/imagens/clear', [
            'as'   => 'painel.projetos.imagens.clear',
            'uses' => 'ProjetosImagensController@clear'
        ]);
        Route::delete('projetos/{projeto}/excluir-imagens/{imagens}', 'ProjetosImagensController@destroyImgs')->name('painel.projetos.excluir-imagens');
        Route::post('projetos/{projeto}/imagens/{imagem}/descricao', 'ProjetosImagensController@updateDescricao')->name('painel.projetos.imagens.descricao');
        Route::resource('projetos.imagens', 'ProjetosImagensController', ['parameters' => ['imagens' => 'projetos_imagens']]);
        Route::resource('projetos-antes-depois', 'ProjetosAntesDepoisController', ['except' => 'show']);
        Route::resource('projetos-antes-depois.imagens', 'ProjetosAntesDepoisImagensController');
        Route::resource('midias', 'MidiasController');
        Route::resource('midias.galeria', 'MidiasGaleriasController');
        Route::get('midias/{galeria}/imagens/clear', [
            'as'   => 'painel.midias.galeria.imagens.clear',
            'uses' => 'MidiasGaleriasImagensController@clear'
        ]);
        Route::resource('midias.galeria.imagens', 'MidiasGaleriasImagensController', ['parameters' => ['imagens' => 'midias_galerias_imagens']]);
        Route::resource('midias.link', 'MidiasLinksController');
        Route::resource('midias.video', 'MidiasVideosController');
        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('usuarios', 'UsuariosController');

        // Limpar caches
        Route::get('clear-cache', function() {
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('cache:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('view:clear');
            $exitCode = Artisan::call('config:cache');

            return 'DONE';
        });
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
        Route::get('esqueci-minha-senha', 'PasswordController@sendEmail')->name('esqueci-minha-senha');
        Route::post('esqueci-minha-senha', 'PasswordController@sendResetLinkEmail')->name('esqueci-minha-senha.post');
        Route::get('reset/{token}', 'PasswordController@showResetForm')->name('password-reset');
        Route::post('reset', 'PasswordController@reset')->name('password-reset.post');
    });
});
