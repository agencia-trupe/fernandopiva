<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';

    public function boot(Router $router)
    {
        $router->model('midias_videos', 'App\Models\MidiaVideo');
        $router->model('midias_links', 'App\Models\MidiaLink');
        $router->model('midias_galerias_imagens', 'App\Models\MidiaGaleriaImagem');
        $router->model('midias_galerias', 'App\Models\MidiaGaleria');
        $router->model('midias', 'App\Models\Midia');
        $router->model('midias_tipos', 'App\Models\MidiaTipo');
        $router->model('projetos_antes_depois_imagens', 'App\Models\ProjetoAntesDepoisImagem');
        $router->model('projetos_antes_depois', 'App\Models\ProjetoAntesDepois');
        $router->model('projetos_imagens', 'App\Models\ProjetoImagem');
        $router->model('projetos', 'App\Models\Projeto');
        $router->model('categorias', 'App\Models\Categoria');
        $router->model('sobre', 'App\Models\Sobre');
        $router->model('banners', 'App\Models\Banner');
        $router->model('contatos_recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('configuracoes', 'App\Models\Configuracao');
        $router->model('usuarios', 'App\Models\User');
        $router->model('politica-de-privacidade', 'App\Models\PoliticaDePrivacidade');
        $router->model('aceite-de-cookies', 'App\Models\AceiteDeCookies');

        parent::boot($router);
    }

    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
