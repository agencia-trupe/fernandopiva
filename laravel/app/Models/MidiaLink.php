<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class MidiaLink extends Model
{
    protected $table = 'midias_links';

    protected $guarded = ['id'];

    public function scopeMidia($query, $id)
    {
        return $query->where('midia_id', $id);
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/midias/link/'
        ]);
    }
}
