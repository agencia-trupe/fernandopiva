<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class ProjetoImagem extends Model
{
    protected $table = 'projetos_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProjeto($query, $id)
    {
        return $query->where('projeto_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => null,
                'height'  => 180,
                'path'    => 'assets/img/projetos/imagens/thumbs/'
            ],
            [
                'width'  => null,
                'height' => 550,
                'upsize'  => true,
                'path'    => 'assets/img/projetos/imagens/grid/'
            ],
            [
                'width'  => 1500,
                'height' => null,
                'path'    => 'assets/img/projetos/imagens/'
            ]
        ]);
    }
}
