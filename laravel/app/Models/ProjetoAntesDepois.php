<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class ProjetoAntesDepois extends Model
{
    protected $table = 'projetos_antes_depois';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ProjetoAntesDepoisImagem', 'projeto_id')->ordenados();
    }

    public function scopeAtivos($query)
    {
        return $query->where('ativo', '=', 1);
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 600,
            'height' => null,
            'path'   => 'assets/img/projetos-antes-depois/'
        ]);
    }
}
