<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class ProjetoAntesDepoisImagem extends Model
{
    protected $table = 'projetos_antes_depois_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeProjeto($query, $id)
    {
        return $query->where('projeto_id', $id);
    }

    public static function uploadImagemVerticalAntes()
    {
        return CropImage::make('imagem_antes', [
            'width'  => 400,
            'height' => 550,
            'path'    => 'assets/img/projetos-antes-depois/imagens/'
        ]);
    }

    public static function uploadImagemVerticalDepois()
    {
        return CropImage::make('imagem_depois', [
            'width'  => 400,
            'height' => 550,
            'path'    => 'assets/img/projetos-antes-depois/imagens/'
        ]);
    }

    public static function uploadImagemHorizontalAntes()
    {
        return CropImage::make('imagem_antes', [
            'width'  => 720,
            'height' => 550,
            'path'    => 'assets/img/projetos-antes-depois/imagens/'
        ]);
    }

    public static function uploadImagemHorizontalDepois()
    {
        return CropImage::make('imagem_depois', [
            'width'  => 720,
            'height' => 550,
            'path'    => 'assets/img/projetos-antes-depois/imagens/'
        ]);
    }
}
