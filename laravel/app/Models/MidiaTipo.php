<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MidiaTipo extends Model
{
    protected $table = 'midias_tipos';

    protected $guarded = ['id'];

    public function midias()
    {
        return $this->hasMany('App\Models\Midia', 'tipo_id')->ordenados();
    }
}
