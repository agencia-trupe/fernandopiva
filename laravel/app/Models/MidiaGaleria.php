<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class MidiaGaleria extends Model
{
    protected $table = 'midias_galerias';

    protected $guarded = ['id'];

    public function scopeMidia($query, $id)
    {
        return $query->where('midia_id', $id);
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\MidiaGaleriaImagem', 'galeria_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/midias/galeria/'
        ]);
    }
}
