<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class MidiaGaleriaImagem extends Model
{
    protected $table = 'midias_galerias_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeGaleria($query, $id)
    {
        return $query->where('galeria_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => null,
                'path'    => 'assets/img/midias/galeria/imagens/thumbs/'
            ],
            [
                'width'  => 1000,
                'height' => null,
                'upsize'  => true,
                'path'    => 'assets/img/midias/galeria/imagens/'
            ]
        ]);
    }
}
