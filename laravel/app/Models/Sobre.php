<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Sobre extends Model
{
    protected $table = 'sobre';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 1000,
            'height' => null,
            'path'   => 'assets/img/sobre/'
        ]);
    }
}
