<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MidiaVideo extends Model
{
    protected $table = 'midias_videos';

    protected $guarded = ['id'];

    public function scopeMidia($query, $id)
    {
        return $query->where('midia_id', $id);
    }
}
