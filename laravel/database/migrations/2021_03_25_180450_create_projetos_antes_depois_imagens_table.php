<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateProjetosAntesDepoisImagensTable extends Migration
{
    public function up()
    {
        Schema::create('projetos_antes_depois_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('projeto_id')->unsigned();
            $table->foreign('projeto_id')->references('id')->on('projetos_antes_depois')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('tipo');
            $table->string('imagem_antes');
            $table->string('imagem_depois');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('projetos_antes_depois_imagens');
    }
}
