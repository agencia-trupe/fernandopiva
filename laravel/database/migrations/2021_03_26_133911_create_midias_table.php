<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateMidiasTable extends Migration
{
    public function up()
    {
        Schema::create('midias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('tipo_id')->unsigned();
            $table->foreign('tipo_id')->references('id')->on('midias_tipos')->onDelete('cascade');
            $table->string('slug');
            $table->string('titulo');
            $table->string('ano');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('midias');
    }
}
