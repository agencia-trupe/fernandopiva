<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateProjetosAntesDepoisTable extends Migration
{
    public function up()
    {
        Schema::create('projetos_antes_depois', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('categoria_id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');
            $table->string('slug');
            $table->string('titulo');
            $table->string('capa');
            $table->boolean('ativo')->default(true);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('projetos_antes_depois');
    }
}
