<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateMidiasGaleriasImagensTable extends Migration
{
    public function up()
    {
        Schema::create('midias_galerias_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('galeria_id')->unsigned();
            $table->foreign('galeria_id')->references('id')->on('midias_galerias')->onDelete('cascade');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('midias_galerias_imagens');
    }
}
