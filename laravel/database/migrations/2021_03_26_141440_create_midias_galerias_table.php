<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateMidiasGaleriasTable extends Migration
{
    public function up()
    {
        Schema::create('midias_galerias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('midia_id')->unsigned();
            $table->foreign('midia_id')->references('id')->on('midias')->onDelete('cascade');
            $table->string('capa');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('midias_galerias');
    }
}
