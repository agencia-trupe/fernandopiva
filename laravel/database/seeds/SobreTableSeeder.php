<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SobreTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('sobre')->insert([
            'imagem' => '',
            'texto'  => '',
        ]);
    }
}
