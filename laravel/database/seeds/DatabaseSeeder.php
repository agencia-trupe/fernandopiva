<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(SobreTableSeeder::class);
        $this->call(CategoriasTableSeeder::class);
        $this->call(MidiasTiposTableSeeder::class);
        $this->call(PoliticaDePrivacidadeTableSeeder::class);
    }
}
