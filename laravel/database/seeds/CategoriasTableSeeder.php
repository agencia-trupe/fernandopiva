<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriasTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('categorias')->insert([
            'id'      => 1,
            'ordem'   => 2,
            'titulo'  => 'Comercial',
        ]);

        DB::table('categorias')->insert([
            'id'      => 2,
            'ordem'   => 3,
            'titulo'  => 'Empreendimentos',
        ]);

        DB::table('categorias')->insert([
            'id'      => 3,
            'ordem'   => 1,
            'titulo'  => 'Residencial',
        ]);

        DB::table('categorias')->insert([
            'id'      => 4,
            'ordem'   => 4,
            'titulo'  => 'Mostras',
        ]);
    }
}
