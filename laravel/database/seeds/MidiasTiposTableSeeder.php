<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MidiasTiposTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('midias_tipos')->insert([
            'id'      => 1,
            'titulo'  => 'Galeria de Imagens',
        ]);

        DB::table('midias_tipos')->insert([
            'id'      => 2,
            'titulo'  => 'Link Externo (Blog/Site/Revista)',
        ]);

        DB::table('midias_tipos')->insert([
            'id'      => 3,
            'titulo'  => 'Video YouTube',
        ]);
    }
}
