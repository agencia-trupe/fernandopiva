<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contato')->insert([
            'nome'        => 'Fernando Piva',
            'email'       => 'contato@trupe.net',
            'telefone'    => '+55 11 3168 1711',
            'endereco'    => 'Av. Nove de Julho, 5593/21 · 01407-200 · São Paulo - SP',
            'instagram'   => 'https://www.instagram.com/fernandopivainteriores/',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.6584598263685!2d-46.680761785383424!3d-23.580706968191308!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5767b064556f%3A0x45c126b08f1eb26!2sAv.%20Nove%20de%20Julho%2C%205593%20-%20Itaim%20Bibi%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2001407-200!5e0!3m2!1spt-BR!2sbr!4v1616619101839!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>',
        ]);
    }
}
